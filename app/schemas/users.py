from typing import Optional
from datetime import datetime

from pydantic import validator, Field, root_validator, EmailStr, conlist

from app.schemas.base import DBBaseModel, BaseModel
from app.schemas.skills import Skill
from app.schemas.achievements import Achievement
from app.schemas.quests import Quest, QuestDetails
from app import utils


class User(DBBaseModel):
    nickname: str
    wallet_address: Optional[str] = None
    level_id: Optional[int]
    level_accumulated_exp: Optional[int]


class UserBase(User):
    skills: list[Optional[Skill]]
    trophies: list[Optional[Achievement]]
    completed_quests: list[Optional[Quest]]


class UserAuth(BaseModel):
    email: str
    password: str


class UserSkills(DBBaseModel):
    skill_id: int
    point: int


class UserCreate(User):
    email: EmailStr
    password: str = Field(min_length=9)

    @validator('password')
    def get_hashed_password(cls, value):
        return utils.get_hashed_password(value)


class UserRegister(DBBaseModel):
    id: Optional[int]
    nickname: str
    email: EmailStr
    password: str = Field(min_length=9)


class UserRegisterCreate(UserRegister):
    is_active: bool = True
    level_id: int = 1
    level_accumulated_exp: int = 0

    @validator('password')
    def get_hashed_password(cls, value):
        return utils.get_hashed_password(value)


class UserSignup(UserCreate):
    completed_quest_id: int
    users_skills: list[Optional[UserSkills]]
    exp_to_next_level: int = Field(alias='experience_reward')

    @root_validator()
    def calculate_exp_reward(cls, values):
        if skills := values.get('users_skills'):
            exp_reward = 0
            for skill in skills:
                exp_reward += skill.point
            values['exp_to_next_level'] = exp_reward
        return values


class UserUpdate(UserCreate):
    id: int


class UpdateNFTTime(DBBaseModel):
    time: datetime = datetime.now()
    zone: str


class UpdateNFTTimeInput(UpdateNFTTime):
    @validator('time')
    def convert_time(cls, value):
        return value.strftime("%m/%d/%y %H:%M:%S")


class UpdateNFTTimeOutput(UpdateNFTTime):
    time: str

    @validator('time')
    def convert_time(cls, value):
        return datetime.strptime(value, '%m/%d/%y %H:%M:%S')


class UserNFTInput(DBBaseModel):
    tx_hash: str
    token_id: str
    from_address: str
    updated_at: UpdateNFTTimeInput


class UserNFTOutput(UserNFTInput):
    updated_at: UpdateNFTTimeOutput


class UserResponse(User):
    id: int
    exp_to_next_level: Optional[int]
    completed_quests: list[Optional[QuestDetails]]
    nfts: Optional[list[UserNFTOutput]]


class SkillRequest(BaseModel):
    skill_id: int


class UserCompleteQuestIn(DBBaseModel):
    experience_reward: int
    skills: list[SkillRequest]


class UserNotActiveResponse(DBBaseModel):
    id: int


class UserNotActiveRequest(DBBaseModel):
    nickname: str


class UserNotActiveCreate(UserNotActiveRequest):
    is_active: bool = False
    level_id: int = 1
    level_accumulated_exp: int = 0


class UserUpdateWalletAddress(DBBaseModel):
    wallet_address: str


class UserUpdateNFT(DBBaseModel):
    nfts: conlist(UserNFTInput, min_items=1, unique_items=True)

    @root_validator
    def uniqueness_check(csl, values):
        nfts = values.get('nfts')
        hash_set = {nft.tx_hash for nft in nfts}
        token_id_set = {nft.token_id for nft in nfts}
        if len(hash_set) != len(nfts) or len(token_id_set) != len(nfts):
            raise ValueError('Not unique values')
        return values
